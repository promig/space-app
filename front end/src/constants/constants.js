export const dangerColorGradient = [
  "#29FF01",
  "#E6FDD2",
  "#CCFF02",
  "#99FE00",
  "#C9FC00",
  "#FFFF01",
  "#FFCC00",
  "#FE9900",
  "#FF6600",
  "#ff313d",
  "#891327"
];
