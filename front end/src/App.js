import React, { useEffect, useRef, useState } from "react";
import { FeatureGroup, GeoJSON, Map, TileLayer } from "react-leaflet";
import "./App.css";
import { Button, Card, CardContent, Divider, Icon } from "semantic-ui-react";
import VegetationModal from "./components/vegetationModal";
import Slider from "rc-slider/es/Slider";
import "rc-slider/assets/index.css";

import { dangerColorGradient } from "./constants/constants";

function App() {
  const dropdownOptions = [
    { key: -1, text: "None", value: -1 },
    { key: 1, text: "Population Index", value: "ppltn_d" },
    { key: 2, text: "Water Index", value: "rat_wtr" },
    { key: 3, text: "Slope Index", value: "slope" }
  ];

  const position = [34.8, -92.2];
  const [data, setData] = useState([]);
  const [county, setCounty] = useState(null);
  const [tooltipCoords, setTooltipCoords] = useState({ left: 0, top: 0 });
  const [humedad, setHumedad] = useState(0);
  const [densidad, setDensidad] = useState(0);
  const [vegetacion, setVegetacion] = useState(0);
  const [topagrafia, setTopografia] = useState(0);
  const [distancia, setDistancia] = useState(0);
  const [agua, setAgua] = useState(0);
  const [mapKey, setMapKey] = useState(0);
  const [showVegetacionModal, setShowVegetacionModal] = useState(false);
  const [selectedDropdownOption, setSelectedDropdownOption] = useState(-1);

  useEffect(() => {
    const request = fetch("arkansas_data.json");

    //this could be made with axios

    request.then((r) => r.json()).then((response) => setData(response.features));
  }, []);

  useEffect(() => {
    console.log("selected", selectedDropdownOption);

    const newData = data.map((d, index) => {
      console.log("de las prppiedades: ", d.properties[selectedDropdownOption]);

      return {
        ...d,
        properties: { ...d.properties, nuevoValor: dangerColorGradient[+d.properties[selectedDropdownOption]] }
      };
    });

    setData(newData);
    setMapKey(Math.random());
  }, [selectedDropdownOption]);

  const handleLocationPress = (values) => {
    setCounty(values);
    console.log(values);
  };

  const handleMouseOver = (e, values) => {
    setCounty(values);
    setTooltipCoords({ left: `${e.originalEvent.pageX}px`, top: `${e.originalEvent.pageY}px`, display: "block" });
  };

  const handleMouseLeave = (e) => {
    setTooltipCoords({ left: 0, top: 0, display: "none" });
  };

  const handleClickVegetacion = () => {
    setShowVegetacionModal(true);
  };

  const handleGenerateData = () => {
    const newData = data.map((d, index) => ({
      ...d,
      properties: {
        ...d.properties,
        nuevoValor: `#${index.toString().padStart(3, "0")}${index.toString().padStart(3, "0")}`
      }
    }));

    console.log(newData);

    setData(newData);
    setMapKey(Math.random());
  };

  return (
    <>
      <div className="main-container">
        <div className="main">
          <Card fluid className="main-card" style={{ overflowY: "scroll" }}>
            <CardContent>
              <Card.Header>
                <h1>Fire Information</h1>
              </Card.Header>
              <Divider clearing />
              <div className="fire-container">
                <div className="fire-information">
                  <h4>Location: 34.496212, -93.057220</h4>
                  <h4>Priority: Low</h4>
                  <h4>Area: 3Km</h4>
                </div>
                <div className="fire-picker">
                  <h4>Choose a fire:</h4>

                  <h5 className="fire-date">Date: 2017 097 21</h5>
                  <h5>Date: 2017 298 21</h5>
                  <h5>Date: 2018 063 00</h5>
                  <h5>Date: 2018 071 21</h5>
                  <h5>Date: 2019 064 21</h5>
                  <h5>Date: 2019 075 21</h5>
                  <h5>Date: 2019 085 21</h5>
                </div>
              </div>
            </CardContent>
          </Card>
        </div>
        <div className="map">
          <div className="risk-container">
            <select value={selectedDropdownOption} onChange={(e) => setSelectedDropdownOption(e.target.value)}>
              {dropdownOptions.map((option) => (
                <option key={option.key} value={option.value}>
                  {" "}
                  {option.text}{" "}
                </option>
              ))}
            </select>
            <div>
              Risk:
              {dangerColorGradient.map((d, index) => (
                <span style={{ background: d, padding: "15px" }}>{index}</span>
              ))}
            </div>
          </div>
          <Card fluid className="map-card">
            <Map
              center={position}
              zoom={7}
              style={{
                height: "100%",
                width: "100%"
              }}
              key={mapKey}
            >
              <TileLayer
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
              />

              <FeatureGroup>
                {data.map((f, index) => (
                  <>
                    <GeoJSON
                      key={index}
                      data={f}
                      onClick={() => handleLocationPress(f.properties)}
                      style={{
                        color: `black`,
                        weight: 2,
                        fillColor: `${f.properties.nuevoValor ? f.properties.nuevoValor : "lightblue"}`,
                        fillOpacity: 0.5
                      }}
                      onMouseOver={(e) => handleMouseOver(e, f.properties)}
                      onMouseOut={(e) => handleMouseLeave(e)}
                    />
                  </>
                ))}
              </FeatureGroup>
            </Map>
          </Card>
        </div>
        <div className="secondary">
          <Card fluid className="secondary-card">
            <CardContent>
              <Card.Header>
                <div className="secondary-title-container">
                  <h1>Variables</h1>
                  <Button animated onClick={handleGenerateData}>
                    <Button.Content visible>Generate</Button.Content>
                    <Button.Content hidden>
                      <Icon name="arrow right" />
                    </Button.Content>
                  </Button>
                </div>
              </Card.Header>
              <Divider clearing />
              <div>
                <div className="variable-container">
                  <h4>Humedad</h4>
                  <div className="input-container">
                    {/*<InputSlider set={setHumedad} />*/}
                    <Slider min={0} max={10} value={humedad} onChange={setHumedad} startPoint={humedad} />
                  </div>
                  <h4>{humedad}%</h4>
                </div>
                <div className="variable-container">
                  <h4>Densidad</h4>
                  <div className="input-container">
                    {/*<InputSlider set={setDensidad} />*/}
                    <Slider min={0} max={10} value={densidad} onChange={setDensidad} startPoint={densidad} />
                  </div>
                  <h4>{densidad}%</h4>
                </div>
                <div className="variable-container">
                  <h4>Vegetacion</h4>
                  <div className="input-container">
                    {/*<InputSlider set={setVegetacion} />*/}
                    <Slider min={0} max={10} value={vegetacion} onChange={setVegetacion} startPoint={vegetacion} />
                  </div>
                  <h4>{vegetacion}%</h4>
                  <div>
                    <Button icon size="mini" color="blue" onClick={handleClickVegetacion}>
                      <Icon name="table" />
                    </Button>
                  </div>
                </div>
                <div className="variable-container">
                  <h4>Topografia</h4>
                  <div className="input-container">
                    {/*<InputSlider set={setTopografia} />*/}
                    <Slider min={0} max={10} value={topagrafia} onChange={setTopografia} startPoint={topagrafia} />
                  </div>
                  <h4>{topagrafia}%</h4>
                  <div>
                    <Button icon size="mini" color="blue">
                      <Icon name="table" />
                    </Button>
                  </div>
                </div>
                <div className="variable-container">
                  <h4>Distancia</h4>
                  <div className="input-container">
                    <Slider min={0} max={10} value={distancia} onChange={setDistancia} startPoint={distancia} />
                  </div>
                  <h4>{distancia}%</h4>
                </div>
                <div className="variable-container">
                  <h4>Agua / Tierra</h4>
                  <div className="input-container">
                    <Slider min={0} max={10} value={agua} onChange={setAgua} startPoint={agua} />
                  </div>
                  <h4>{agua}%</h4>
                </div>
              </div>
            </CardContent>
          </Card>
        </div>
      </div>
      <div className="tooltip" style={{ ...tooltipCoords }}>
        {county && (
          <>
            <h3>{county && county.NAME}</h3>​<p>Population Density Index: {(+county.ppltn_d).toFixed(0)} </p>
            <p>Name: {county.NAME}</p>
            <p>Water Index: {(+county.rat_wtr).toFixed(0)}</p>
            <p>Slope Index: {(+county.slope).toFixed(0)}</p>
            <p>Geo Id: {(+county.GEOID).toFixed(0)}</p>
          </>
        )}
      </div>

      <VegetationModal
        setShowVegetacionModal={setShowVegetacionModal}
        showVegetacionModal={showVegetacionModal}
        save={console.log}
      />
    </>
  );
}

export default App;
