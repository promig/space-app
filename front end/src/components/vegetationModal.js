import React, { useRef, useState } from "react";
import { Button, Icon, Input, Modal, Table } from "semantic-ui-react";

export default function ({ showVegetacionModal, setShowVegetacionModal, save }) {
  const [data, setData] = useState([
    { name: "Forest", value: 5 },
    { name: "shrubland", value: 5 },
    { name: "grass/ Pasture", value: 5 },
    { name: "peppers", value: 5 },
    { name: "Soybean", value: 5 },
    { name: "Barren", value: 5 },
    { name: "Pecans", value: 5 },
    { name: "Developed / Open Space", value: 5 },
    { name: "Rice", value: 5 },
    { name: "Corn", value: 5 },
    { name: "Cotton", value: 5 },
    { name: "Almonds", value: 5 },
    { name: "Developed/ low intensity", value: 5 },
    { name: "Others", value: 5 }
  ]);

  const handleChangeValue = (index, value) => {
    const dataCopy = [...data];
    dataCopy[index].value = value;
    setData(dataCopy);
  };

  const handleSave = () => {
    save(data);
    setShowVegetacionModal(false);
  };

  return (
    <Modal onClose={() => setShowVegetacionModal(false)} open={showVegetacionModal}>
      <Modal.Header>Vegetation Index</Modal.Header>
      <Modal.Content>
        <Modal.Description>
          <Table celled striped collapsing style={{ minWidth: "50%" }}>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell colSpan="2">Vegetation Values</Table.HeaderCell>
              </Table.Row>
            </Table.Header>

            <Table.Body>
              {data.map((d, i) => (
                <Table.Row>
                  <Table.Cell>{d.name}</Table.Cell>
                  <Table.Cell collapsing textAlign="right">
                    <Input type="number" value={d.value} onChange={(e) => handleChangeValue(i, e.target.value)} />
                  </Table.Cell>
                </Table.Row>
              ))}
            </Table.Body>
          </Table>
        </Modal.Description>
      </Modal.Content>
      <Modal.Actions>
        <Button color="black" onClick={() => setShowVegetacionModal(false)}>
          Close
        </Button>
        <Button color="green" onClick={handleSave}>
          Save
        </Button>
      </Modal.Actions>
    </Modal>
  );
}
