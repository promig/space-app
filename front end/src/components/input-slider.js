import React, { useRef, useState } from "react";

export default function ({ set }) {
  const speedRef = useRef();
  const speedBarRef = useRef();

  let mouseDown = false;
  let value = 0;

  function handleMouseMove(e) {
    // const event = e;
    if (!mouseDown) return;
    //can't do a => arrow function cause this is lost
    const position = e.pageX - speedRef.current.offsetLeft;
    console.log("position", e.pageX, position, speedRef.current.offsetLeft, speedRef.current.offsetWidth);
    //
    const percentage = position / speedRef.current.offsetWidth;
    //
    const width = Math.round(percentage * 100);

    speedBarRef.current.style.width = `${width}%`;
    value = width;
  }

  function handleMouseUp() {
    mouseDown = false;

    let setValue = value;

    if (value < 0) {
      setValue = 0;
    } else if (value > 100) {
      setValue = 100;
    }

    set(setValue);
  }

  return (
    <div className="range-container">
      <div
        className="range"
        onMouseDown={() => (mouseDown = true)}
        onMouseUp={handleMouseUp}
        onMouseMove={handleMouseMove}
        ref={speedRef}
      >
        <div className="range-bar" ref={speedBarRef}>
          &nbsp;
        </div>
      </div>
    </div>
  );
}
