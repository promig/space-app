# space-app

Nasa space apps challenge 2020

## Data structure

To load the data, on the root repository of the project, one needs to create two subdirectories:

- tif
- bmp

The tif directly includes all the tif images used as input for the model, while bmp contains the mask bmp images.
