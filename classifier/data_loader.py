import cv2
import numpy as np
import os
import rasterio
import torch
import torch.utils
from PIL import Image
from torchvision import transforms as transforms


class ImprovementDataset(torch.utils.data.Dataset):
    def __init__(self, root_dir, image_size=256, transform=None):
        self.input_path = os.path.abspath(root_dir)
        self.transform = transform
        self.bmp_images_dir = os.path.join(self.input_path, 'bmp')
        self.tif_images_dir = os.path.join(self.input_path, 'tif')
        self.bmp_images = list(os.listdir(self.bmp_images_dir))
        self.image_size = image_size

    def __len__(self):
        return len(self.bmp_images)

    def __getitem__(self, item):
        bmp_image = self.bmp_images[item]
        base_name = bmp_image[:-3]
        jpg_image = base_name + 'tif'
        bmp_img_path = os.path.join(self.bmp_images_dir, bmp_image)
        tif_img_path = os.path.join(self.tif_images_dir, jpg_image)

        source_img = torch.tensor(tif_loader(tif_img_path, self.image_size), dtype=torch.float)
        target_img = pil_loader(bmp_img_path, transform_r=False)

        if self.transform:
            target_img = self.transform(target_img)

        return source_img, target_img, base_name


class PredictDataset(torch.utils.data.Dataset):
    def __init__(self, img_list, data_dir):
        self.img_list = img_list
        self.data_dir = data_dir

    def __len__(self):
        return len(self.img_list)

    def __getitem__(self, item):
        img_name = self.img_list[item]
        img_path = os.path.join(self.data_dir, img_name)

        img_load = torch.tensor(tif_loader(img_path), dtype=torch.float)
        return img_name, img_load


def pil_loader(path, transform_r=True):
    with open(path, 'rb') as f:
        img = Image.open(f)
        if transform_r:
            return img.convert('RGB')
        return img.convert('L')


def tif_loader(img_path, image_size=256):
    with rasterio.open(img_path, 'r') as data:
        return cv2.resize(np.moveaxis(data.read([1]), 0, -1), (image_size, image_size)).reshape(1, image_size,
                                                                                                image_size)


def load_data(root_path, transform=None, image_size=256, batchsize=64):
    if transform is None:
        transform = transforms.Compose([transforms.Resize((image_size, image_size)), transforms.ToTensor()])

    my_data_set = ImprovementDataset(root_dir=root_path, transform=transform)
    data_loader = torch.utils.data.DataLoader(my_data_set, batch_size=batchsize)
    return data_loader
