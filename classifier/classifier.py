from __future__ import print_function
import torch
import torch.nn as nn
import torch.nn.parallel
import torch.utils.data

# custom weights initialization


def weights_init(m):
    classname = m.__class__.__name__
    if classname.find('Conv') != -1:
        torch.nn.init.normal_(m.weight, 0.0, 0.02)
    elif classname.find('BatchNorm') != -1:
        torch.nn.init.normal_(m.weight, 1.0, 0.02)
        torch.nn.init.zeros_(m.bias)


class UnetNetwork(nn.Module):
    def __init__(self):
        super(UnetNetwork, self).__init__()
        batch_momentum = 0.01
        self.batch_conv = nn.Sequential(
            nn.BatchNorm2d(24, momentum=batch_momentum),
            nn.Conv2d(24, 24, kernel_size=(3, 3), stride=(1, 1), padding=1),
            nn.ReLU()
        )

        self.batch_conv_up_1 = nn.Sequential(
            nn.BatchNorm2d(48, momentum=batch_momentum),
            nn.Conv2d(48, 32, kernel_size=(3, 3), stride=(1, 1), padding=1),
            nn.ReLU()
        )

        self.batch_conv_up_2 = nn.Sequential(
            nn.BatchNorm2d(32, momentum=batch_momentum),
            nn.Conv2d(32, 24, kernel_size=(3, 3), stride=(1, 1), padding=1),
            nn.ReLU()
        )

        self.batch_conv_trans = nn.Sequential(
            nn.BatchNorm2d(24, momentum=batch_momentum),
            nn.ConvTranspose2d(24, 24, kernel_size=(3, 3), stride=(2, 2), padding=1, output_padding=1),
            nn.ReLU()
        )

        self.conv2d = nn.Conv2d(24, 24, kernel_size=(3, 3), stride=(1, 1), padding=1)
        self.conv2d_initial = nn.Conv2d(1, 24, kernel_size=(3, 3), stride=(1, 1), padding=1)
        self.max_pool = nn.MaxPool2d((2, 2), (2, 2))
        self.output_conv = nn.Sequential(
            nn.Conv2d(24, 1, kernel_size=(1, 1), stride=(1, 1)),
            nn.Sigmoid()
        )

    def forward(self, input_):
        x = self.conv2d_initial(input_)
        c1 = self.batch_conv(x)
        x = self.conv2d(x)
        x = self.max_pool(x)

        down_layers = []
        for _ in range(2):
            x = self.batch_conv(x)
            x = self.batch_conv(x)
            down_layers.append(x)
            x = self.batch_conv(x)
            x = self.max_pool(x)

        x = self.batch_conv(x)
        x = self.batch_conv(x)
        x = self.batch_conv_trans(x)

        down_layers.reverse()
        x = torch.cat((x, down_layers[0]), dim=1)
        x = self.batch_conv_up_1(x)
        x = self.batch_conv_up_2(x)
        x = self.batch_conv_trans(x)

        x = torch.cat((x, down_layers[1]), dim=1)
        x = self.batch_conv_up_1(x)
        x = self.batch_conv_up_2(x)
        x = self.batch_conv_trans(x)

        x = torch.cat((x, c1), dim=1)

        x = self.batch_conv_up_1(x)
        x = self.batch_conv_up_2(x)

        return self.output_conv(x)
