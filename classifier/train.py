import argparse
import matplotlib.pyplot as plt
import torch
import torch.optim as optim
import torch.nn as nn
from torchsummary import summary

from classifier import UnetNetwork, weights_init
from data_loader import load_data


def train():
    parser = argparse.ArgumentParser()
    parser.add_argument('--dataroot', type=str, help='path to dataset')
    parser.add_argument('--imageSize', type=int, default=256, help='the height / width of the input image to network')
    parser.add_argument('--niter', type=int, default=25, help='number of epochs to train for')
    parser.add_argument('--lr', type=float, default=0.0001, help='learning rate, default=0.0002')
    parser.add_argument('--beta1', type=float, default=0.9, help='beta1 for adam. default=0.5')
    parser.add_argument('--batchsize', type=int, default=64, help='batch size')
    parser.add_argument('--manualSeed', type=int, help='manual seed')

    opt_ = parser.parse_args()
    image_size = opt_.imageSize

    net = UnetNetwork()
    summary(net, (1, image_size, image_size))
    net.apply(weights_init)
    criterion = nn.BCELoss()
    optimizer = optim.Adam(net.parameters(), lr=opt_.lr, betas=(opt_.beta1, 0.999), eps=1e-07)
    data_loader = load_data(opt_.dataroot, batchsize=opt_.batchsize)
    losses = []
    print('d_load: ', len(data_loader))
    for epoch in range(opt_.niter):
        print('epoch: ', epoch)
        for i, data in enumerate(data_loader, 0):
            input_image, mask_image, image_name = data
            optimizer.zero_grad()
            output = net(input_image)
            loss = criterion(output, mask_image)
            loss.backward()
            optimizer.step()
            losses.append(loss.item())

        plt.plot(losses)
        plt.show()

    plt.plot(losses)
    plt.show()
    torch.save(net.state_dict(), 'net_result.pth')


if __name__ == '__main__':
    train()
