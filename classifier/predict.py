import torch
import numpy as np
from torchsummary import summary
from PIL import Image
import argparse

from classifier import UnetNetwork
from data_loader import PredictDataset

data_sets = ["time-20170972116118-loc--92.6_32.8_-91.4_33.6.tif", "time-20170972116118-loc--93.2_34.6_-91.9_35.9.tif",
             "time-20172982015393-loc--94.1_31.4_-88.9_34.8.tif", "time-20180622315381-loc--95.2_35.3_-92.85_37.3.tif",
             "time-20180712215420-loc--93.8_35.4_-91.4_37.0.tif", "time-20190642100306-loc--95.0_33.5_-91.6_36.3.tif",
             "time-20190752015366-loc--95.2_34.3_-92.1_36.8.tif", "time-20190852215341-loc--94.1_35.0_-93.4_36.0.tif", 
             ]


def predict(img_load, image_size=256):
    net = UnetNetwork()
    net.batch_size = 1
    summary(net, (1, image_size, image_size))
    net.load_state_dict(torch.load('net_result.pth'))
    prediction = net(img_load)
    return prediction


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--dataroot', type=str, help='path to dataset')

    image_size_ = 256

    opt_ = parser.parse_args()
    threshold = 0.45
    predict_data_set = PredictDataset(data_sets, opt_.dataroot)
    predict_data_loader = torch.utils.data.DataLoader(predict_data_set, batch_size=1)
    for image_data in predict_data_loader:
        image_name, image = image_data
        prediction_tensor = predict(image)
        prediction_array = prediction_tensor.detach().numpy().reshape(image_size_, image_size_)
        prediction_array = np.where(prediction_array > threshold, 1, 0)
        plot_array = prediction_array.astype('uint8') * 255
        im = Image.fromarray(plot_array)
        csv_save = image_name[0][:-3] + 'csv'
        im.save(image_name[0][:-3] + 'jpg')
        np.savetxt(csv_save, prediction_array, delimiter=',')
